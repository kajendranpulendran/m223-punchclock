package ch.zli.m223.punchclock;

import ch.zli.m223.punchclock.repository.SectionRepository;
import ch.zli.m223.punchclock.service.CostCenterService;
import ch.zli.m223.punchclock.service.SectionService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;


@SpringBootApplication
public class PunchclockApplication {
	public static void main(String[] args) {
		SpringApplication.run(PunchclockApplication.class, args);
		SectionService.insertSection("Informatik");
		SectionService.insertSection("Finanzen");
		SectionService.insertSection("Security");

		CostCenterService.insertCostCenter("CH789 Pause");
		CostCenterService.insertCostCenter("CH485 Arbeiten");
		CostCenterService.insertCostCenter("CH103 Meeting");

	}
	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}







}
