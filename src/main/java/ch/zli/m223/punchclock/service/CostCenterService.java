package ch.zli.m223.punchclock.service;

import ch.zli.m223.punchclock.domain.CostCenter;
import ch.zli.m223.punchclock.repository.CostCenterRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CostCenterService {
    private static CostCenterRepository costCenterRepository;

    public CostCenterService(CostCenterRepository costCenterRepository) {
        this.costCenterRepository = costCenterRepository;
    }

    public CostCenter createCostCenter(CostCenter costCenter) {
        return costCenterRepository.saveAndFlush(costCenter);
    }

    public List<CostCenter> findAll() {
        return costCenterRepository.findAll();
    }

    public void deleteCostCenter(CostCenter costCenter) { costCenterRepository.delete(costCenter); }

    public static void insertCostCenter(String costCenterName) {
        costCenterRepository.insertCostCenter(costCenterName);
    }
}
