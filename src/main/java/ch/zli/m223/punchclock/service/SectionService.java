package ch.zli.m223.punchclock.service;

import ch.zli.m223.punchclock.domain.Section;
import ch.zli.m223.punchclock.repository.SectionRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SectionService {
    private static SectionRepository sectionRepository;

    public SectionService(SectionRepository sectionRepository) {
        this.sectionRepository = sectionRepository;
    }

    public Section createSection(Section section) {
        return sectionRepository.saveAndFlush(section);
    }

    public List<Section> findAll() {
        return sectionRepository.findAll();
    }

    public void deleteSection(Section section) {
        sectionRepository.delete(section);
    }

    public static void insertSection(String sectionName) {
        sectionRepository.insertSection(sectionName);
    }
}
