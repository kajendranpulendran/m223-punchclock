package ch.zli.m223.punchclock.service;

import ch.zli.m223.punchclock.domain.Entry;
import ch.zli.m223.punchclock.repository.EntryRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EntryService {
    private EntryRepository entryRepository;

    public EntryService(EntryRepository entryRepository) {
        this.entryRepository = entryRepository;
    }

    public Entry createEntry(Entry entry) {
        return entryRepository.saveAndFlush(entry);
    }

    public List<Entry> findAll() {
        return entryRepository.findAll();
    }

    public Entry deleteEntry(long id) {
        Entry entry = findById(id);
        entryRepository.delete(entry);
        return entry;
    }

    public Entry findById(long id) {
        Optional<Entry> entry = entryRepository.findById(id);
        if (entry.isPresent()) {
            return entry.get();
        } else {
            throw new RuntimeException(("Konte nicht gefuden werden."));
        }
    }

}
