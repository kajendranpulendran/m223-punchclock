package ch.zli.m223.punchclock.service;

import ch.zli.m223.punchclock.domain.Entry;
import ch.zli.m223.punchclock.domain.User;
import ch.zli.m223.punchclock.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    private UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User createUser(User user) {
        return userRepository.saveAndFlush(user);
    }

    public List<User> findAll() {
        return userRepository.findAll();
}

    public void deleteUser(User user) {
        userRepository.delete(user);
    }

    public User deletUserById(long id) {
        User user = findById(id);
        userRepository.delete(user);
        return user;
//        entryRepository.findById(id);
    }

    public User findById(long id) {
        Optional<User> entry = userRepository.findById(id);
        if (entry.isPresent()) {
            return entry.get();
        } else {
            throw new RuntimeException(("Konte nicht gefuden werden."));
        }
    }

    public void updateService(User username, long id) {
        Optional<User> user = userRepository.findById(id);
        if (user.isPresent()) {
            username.setId(id);
            userRepository.save(username);
        } else {
            throw new RuntimeException(("Konte nicht gefuden werden."));
        }
    }
}
