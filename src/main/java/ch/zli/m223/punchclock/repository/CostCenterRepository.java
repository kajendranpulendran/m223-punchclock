package ch.zli.m223.punchclock.repository;

import ch.zli.m223.punchclock.domain.CostCenter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface CostCenterRepository extends JpaRepository<CostCenter, Long> {
    @Transactional
    @Query(value = "INSERT INTO COST_CENTER (Cost_Center_Name) VALUES (:costCenterName)", nativeQuery = true)
    @Modifying
    void insertCostCenter(@Param("costCenterName") String costCenterName);
}
