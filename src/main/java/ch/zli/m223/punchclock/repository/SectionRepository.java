package ch.zli.m223.punchclock.repository;

import ch.zli.m223.punchclock.domain.Section;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface SectionRepository extends JpaRepository<Section, Long> {
    @Transactional
    @Query(value = "INSERT INTO Section (SECTION_NAME) VALUES (:sectionName)", nativeQuery = true)
    @Modifying
    void insertSection(@Param("sectionName") String sectionName);
}
