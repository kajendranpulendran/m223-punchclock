package ch.zli.m223.punchclock.controller;

import ch.zli.m223.punchclock.domain.Section;
import ch.zli.m223.punchclock.service.SectionService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/section")
public class SectionController {
    private SectionService sectionService;

    public SectionController(SectionService sectionService) {
        this.sectionService = sectionService;
    }

    /**
     * Get all Section
     * @return Sections
     */
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Section> getAllSection() {
        return sectionService.findAll();
    }

    /**
     * Create new Section
     * @param section
     * @return Section
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Section sectionService(@Valid @RequestBody Section section) {
        return sectionService.createSection(section);
    }

    /**
     * Delete Sction
     * @param section
     */
    @DeleteMapping
    @ResponseStatus(HttpStatus.OK)
    public void deleteSction(@Valid @RequestBody Section section) {
        sectionService.deleteSection(section);
    }
}
