package ch.zli.m223.punchclock.controller;

import ch.zli.m223.punchclock.domain.Entry;
import ch.zli.m223.punchclock.domain.User;
import ch.zli.m223.punchclock.service.UserService;
import com.fasterxml.jackson.databind.util.JSONPObject;
import org.apache.commons.codec.binary.Base64;
import org.json.JSONException;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.json.JSONObject;

import javax.persistence.PostUpdate;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.ws.rs.PUT;
import java.util.List;

import static ch.zli.m223.punchclock.security.SecurityConstants.HEADER_STRING;
import static ch.zli.m223.punchclock.security.SecurityConstants.TOKEN_PREFIX;

@RestController
@RequestMapping("/users")
public class UserController {
    private UserService userService;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public UserController(UserService userService, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userService = userService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    /**
     * Get all Users
     * @return Users
     */
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<User> getAllUsers() {
        return userService.findAll();
    }

    /**
     * Get Current User which is logged in
     * @param request
     * @return
     * @throws JSONException
     */
    @GetMapping
    @RequestMapping("/current")
    @ResponseStatus(HttpStatus.OK)
    public User currentUser(@Valid HttpServletRequest request) throws JSONException {
        String jwt = request.getHeader(HEADER_STRING).substring(TOKEN_PREFIX.length());
        String[] split_string = jwt.split("\\.");
        String base64EncodedBody = split_string[1];

        String payload = new String(new Base64(true).decode(base64EncodedBody));
        JSONObject object = new JSONObject(payload);
        String username = object.getString("sub");
        User matchingUser = userService.findAll().stream().filter(t -> t.getUsername().equals(username)).findFirst().get();
        return matchingUser;
    }

    /**
     * Create new User
     * @param user
     * @return
     */
    @RequestMapping("/sign-up")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public User createUser(@Valid @RequestBody User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        return userService.createUser(user);
    }

    /**
     * Delte user
     * @param user
     */
    @DeleteMapping
    @ResponseStatus(HttpStatus.OK)
    public void deleteUser(@Valid @RequestBody User user) {
        userService.deleteUser(user);
    }

    /**
     * Update User by id
     * @param id
     * @param user
     */
    @PutMapping({"/{id}"})
    @ResponseStatus(HttpStatus.OK)
    public void updateUser(@PathVariable("id") long id, @RequestBody User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userService.updateService(user, id);
    }

    /**
     *Delete User by id
     * @param id
     */
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteEntry(@PathVariable long id) {
        userService.deletUserById(id);
    }
}
