package ch.zli.m223.punchclock.controller;

import ch.zli.m223.punchclock.domain.CostCenter;
import ch.zli.m223.punchclock.service.CostCenterService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/costcenters")
public class CostCenterController {
    private CostCenterService costCenterService;

    public CostCenterController(CostCenterService costCenterService) {
        this.costCenterService = costCenterService;
    }

    /**
     * Get all CostCenter
     * @return costCenters
     */
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<CostCenter> getAllCostCenters() {
        return costCenterService.findAll();
    }

    /**
     *Create costCenter
     * @param costCenter
     * @return CostCenter
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CostCenter createCostCenter(@Valid @RequestBody CostCenter costCenter) {
        return costCenterService.createCostCenter(costCenter);
    }

    /**
     * Delete CostCenter
     * @param costCenter
     */
    @DeleteMapping
    @ResponseStatus(HttpStatus.OK)
    public void deleteCostCenter(@Valid @RequestBody CostCenter costCenter) {
        costCenterService.deleteCostCenter(costCenter);
    }
}
