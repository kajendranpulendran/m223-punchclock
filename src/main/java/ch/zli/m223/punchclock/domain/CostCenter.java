package ch.zli.m223.punchclock.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
public class CostCenter {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String costCenterName;

    @JsonBackReference(value = "cost")
    @OneToMany(mappedBy = "cost", fetch = FetchType.EAGER)
    private List<Entry> entries;

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public String getCostCenterName() { return costCenterName; }

    public void setCostCenterName(String costCenterName) { this.costCenterName = costCenterName; }

    public List<Entry> getEntries() { return entries; }

    public void setEntries(List<Entry> entries) { this.entries = entries; }
}
